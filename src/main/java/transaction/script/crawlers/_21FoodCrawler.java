package transaction.script.crawlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class _21FoodCrawler {
	public static void main(String[] args) {
		String baseUrl = "http://www.21food.com";
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		int rowNumber = 0;
		for (int i = 1; i <= 12; i++) {
			try {
				Document document = Jsoup.parse(IOUtils.toString(new URL("http://www.21food.com/company/search_keys-meat+%26+poultry_c-Germany-p"+i+".html")));
				TimeUnit.MILLISECONDS.sleep(500);
				int index = 0;
				while (true) {
					String businessRange = "", products = "", address = "", mobile = "", otherMobile = "", bizName = "", website = "";
					Row row = sheet.createRow(rowNumber);
					rowNumber++;
					try {
						String text = document.getElementsByClass("link_t1").get(index).attr("href");
						Document linkDocument = null;
						try {
							linkDocument = Jsoup.parse(IOUtils.toString(new URL(baseUrl + text)));
						} catch (Exception e1) {
							continue;
						}
						TimeUnit.MILLISECONDS.sleep(500);
						//System.out.println(baseUrl + text);
						bizName = linkDocument.getElementsByClass("webname").text();
						System.out.println("\n\n\n" + bizName);
						Elements rows = linkDocument.getElementsByClass("marg_x15");
						for (Element element : rows) {
							if (element.toString().contains("Basic Information</span>")) {
								int innerIndex = 0;
								while (true) {
									try {
										Element subElement = element.getElementsByTag("tr").get(innerIndex);
										if (subElement.getElementsByTag("td").get(0).text().toString().contains("Business Range:")) {
											businessRange = subElement.getElementsByTag("td").get(0).nextElementSibling().text().trim();
											System.out.println("business range is::" + businessRange);
										}
										if (subElement.getElementsByTag("td").get(0).text().toString().contains("Main Products:")) {
											products = subElement.getElementsByTag("td").get(0).nextElementSibling().text().trim();
											System.out.println("products are::" + products);
										}
										innerIndex++;
									} catch (IndexOutOfBoundsException e) {
										//e.printStackTrace();
										break;
									}
								}
							}
						}
						Elements elements = linkDocument.getElementsByAttributeValue("bgcolor", "#F3F3F3").get(0).getElementsByTag("td");
						for (int j = 0; j < elements.size(); j++) {
							if (elements.get(j).text().toString().contains("Address")) {
								j += 1;
								address = elements.get(j).text();
								System.out.println(address);
							}
							if (elements.get(j).text().toString().contains("Tel")) {
								j += 1;
								mobile = elements.get(j).text();
								System.out.println(mobile);
							}
							if (elements.get(j).text().toString().contains("Fax")) {
								j += 1;
								otherMobile = elements.get(j).text();
								System.out.println(otherMobile);
							}
							if (elements.get(j).text().toString().contains("Website")) {
								j += 1;
								website = elements.get(j).toString().split("'")[1];
								System.out.println(website);
							}
						}
						index++;
					} catch (IndexOutOfBoundsException e) {
						break;
					}
					row.createCell(0).setCellValue(bizName);
					row.createCell(1).setCellValue(mobile);
					if (products.isEmpty())
						row.createCell(2).setCellValue(businessRange);
					else
						row.createCell(2).setCellValue(products);
					row.createCell(3).setCellValue(address);
					row.createCell(4).setCellValue(website);
					row.createCell(5).setCellValue(otherMobile);
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (workbook != null) {
			try (FileOutputStream outputStream = new FileOutputStream(new File("/home/abhi/Desktop/test/Germany_Meat&Poultry_Data.xlsx"));) {
				workbook.write(outputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}