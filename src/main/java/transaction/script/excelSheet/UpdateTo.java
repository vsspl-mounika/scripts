package transaction.script.excelSheet;

public class UpdateTo {
	private Object _id;
	private String name;
	private String bizName;
	private String bizTypeid;
	private String bizId;
	private String profileId;
	private String locationId;
	private String mobileNumber;
	private String countryCode;
	private String status;
	private String prfCreatedDate;

	public Object get_id() {
		return _id;
	}

	public void set_id(Object _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBizName() {
		return bizName;
	}

	public void setBizName(String bizName) {
		this.bizName = bizName;
	}

	public String getBizTypeid() {
		return bizTypeid;
	}

	public void setBizTypeid(String bizTypeid) {
		this.bizTypeid = bizTypeid;
	}

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getMobileNum() {
		return mobileNumber;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNumber = mobileNum;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPrfCreatedDate() {
		return prfCreatedDate;
	}

	public void setPrfCreatedDate(String prfCreatedDate) {
		this.prfCreatedDate = prfCreatedDate;
	}

}
