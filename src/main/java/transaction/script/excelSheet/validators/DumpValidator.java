package transaction.script.excelSheet.validators;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.bson.types.ObjectId;
import org.glassfish.jersey.client.ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.models.MasterCollectionTo;
import transaction.script.models.PlaceTo;
import transaction.script.models.ProductTo;
import transaction.script.models.RegistrationTo;
import transaction.script.platform.utils.HsqlDBUtil;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.WriteResult;

public class DumpValidator extends Thread {
	public static final Logger LOGGER = LoggerFactory.getLogger(DumpValidator.class);
	static Client client = ClientBuilder.newClient();
	static DB db = null;
	static DBCollection dumpingCollection = null;
	static DBCollection masterCollection = null;
	static Gson gson = null;
	static Connection hsqlConnection = null;
	static volatile HashMap<String, ProductTo> productsMap = new HashMap<String, ProductTo>();
	static volatile HashMap<String, PlaceTo> locationsMap = new HashMap<String, PlaceTo>();
	static volatile Set<String> processedMobileNumbers;
	static volatile Long currentDummyMobileNumber;
	static Pattern pattern = Pattern.compile("[^a-z A-Z \\s]*");
	static volatile HashMap<String, String> sheetDetails = new HashMap<String, String>();
	static volatile boolean shouldICleanUpAndStop = false;
	static {
		try {
			client.property(ClientProperties.CONNECT_TIMEOUT, 60000);
			client.property(ClientProperties.READ_TIMEOUT, 60000);
			hsqlConnection = HsqlDBUtil.dbConnection;
			db = MongoDBUtil.getConnection();
			dumpingCollection = db.getCollection("1OCTDUMP");//EligibleSept28test
			masterCollection = db.getCollection("masterCollectionOCT1");//abhimastertest
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("Unable to initialise scripts");
			e.printStackTrace();
			System.exit(1);
		}
	}
	static int i = 0;

	public static void main(String... ar) {
		@SuppressWarnings("unchecked")
		List<String> mobileNumbersList = masterCollection.distinct("mobileNum");
		processedMobileNumbers = new HashSet<String>(mobileNumbersList);
		DBCursor cursor = dumpingCollection.find(new BasicDBObject("status", "FAIL_DUP_MOBILE"));
		while (cursor.hasNext()) {
			RegistrationTo objRegistrationTo = gson.fromJson(cursor.next().toString(), RegistrationTo.class);
			objRegistrationTo.getLocationTo().getCountryId();
			validateAndProcessData(objRegistrationTo);
		}
	}

	private static void validateAndProcessData(RegistrationTo objRegistrationTo) {
		objRegistrationTo.setMobileNumber(getDummyMobileNumber(objRegistrationTo.getMobileTelecomCode()));
		objRegistrationTo.setStatus(AppConstants.IN_QUEUE);
		WriteResult result = dumpingCollection.update(new BasicDBObject("_id", objRegistrationTo.get_id().toString()), new BasicDBObject("$set", new BasicDBObject("mobileNumber",
				objRegistrationTo.getMobileNumber()).append("status", objRegistrationTo.getStatus())));
		if (result.getError() != null) {
			System.out.println(gson.toJson(objRegistrationTo));
			System.out.println(result.getError());
			System.out.println(result.getN());
			System.exit(0);
		}
		MasterCollectionTo objMasterCollectionTo = new MasterCollectionTo();
		objMasterCollectionTo.set_id(new ObjectId().toString());
		objMasterCollectionTo.setBizId(null);
		objMasterCollectionTo.setBizName(objRegistrationTo.getBusinessName());
		objMasterCollectionTo.setBizTypeId(objRegistrationTo.getBusinessTypeId());
		objMasterCollectionTo.setLocationId(objRegistrationTo.getLocationTo().getPlaceId());
		objMasterCollectionTo.setMobileNum(objRegistrationTo.getMobileNumber());
		objMasterCollectionTo.setCountryCode(objRegistrationTo.getMobileTelecomCode());
		objMasterCollectionTo.setName(objRegistrationTo.getName());
		objMasterCollectionTo.setProfileId(null);
		objMasterCollectionTo.setStatus(AppConstants.IN_QUEUE);
		objMasterCollectionTo.setPrfCreatedDate(null);
		WriteResult result1 = masterCollection.insert(gson.fromJson(gson.toJson(objMasterCollectionTo), BasicDBObject.class));
		if (result1.getError() != null) {
			System.out.println(gson.toJson(objRegistrationTo));
			System.out.println(gson.toJson(objMasterCollectionTo));
			System.out.println(result1.getError());
			System.out.println(result1.getN());
			System.exit(0);
		}
		synchronized (processedMobileNumbers) {
			processedMobileNumbers.add(objRegistrationTo.getMobileNumber());
		}
		LOGGER.error((i++) + " data is processed.");
	}

	private static synchronized String getDummyMobileNumber(String mobileTelecomCode) {
		if (currentDummyMobileNumber == null) {
			try (Statement stmt = hsqlConnection.createStatement(); ResultSet resultSet = stmt.executeQuery("SELECT CURRENTNUMBER FROM DUMMY_NUMBERS;")) {
				while (resultSet.next()) {
					currentDummyMobileNumber = (long) resultSet.getInt("CURRENTNUMBER");
				}
			} catch (Exception e) {
				LOGGER.error("Error in fetching dummy mobile number");
				e.printStackTrace();
			}
		}
		String dummyNumber;
		do {
			currentDummyMobileNumber++;
			dummyNumber = currentDummyMobileNumber.toString();
			while (dummyNumber.length() != 10) {
				dummyNumber = "0" + dummyNumber;
			}
			dummyNumber = mobileTelecomCode + dummyNumber;
		} while (processedMobileNumbers.contains(dummyNumber));
		updateDummyMobileNumber();
		return dummyNumber;
	}

	private static void updateDummyMobileNumber() {
		try (PreparedStatement prepStmt = hsqlConnection.prepareStatement("UPDATE DUMMY_NUMBERS SET CURRENTNUMBER = " + currentDummyMobileNumber + ";")) {
			prepStmt.executeUpdate();
		} catch (Exception e) {
			LOGGER.error("Error in updating dummy number.");
			e.printStackTrace();
		}
	}
}