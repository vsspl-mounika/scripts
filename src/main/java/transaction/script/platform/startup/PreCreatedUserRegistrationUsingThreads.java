package transaction.script.platform.startup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.models.RegistrationTo;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class PreCreatedUserRegistrationUsingThreads implements Runnable {
	public static final Logger LOGGER = LoggerFactory.getLogger(PreCreatedUserRegistrationUsingThreads.class);
	static PreCreatedUserRegistrationUsingThreads classObj = new PreCreatedUserRegistrationUsingThreads();
	static BlockingQueue<RegistrationTo> queue = new LinkedBlockingQueue<RegistrationTo>();
	private static Client client = ClientBuilder.newClient();
	static WebTarget webTarget = client.target("http://52.2.242.72:8080/rest/v1/profiles/otpusersignup");//kalgudi.com with public IP address
	//static WebTarget webTarget = client.target("https://www.kalgudi.com/rest/v1/profiles/otpusersignup");
	//static WebTarget webTarget = client.target("http://www.devkalgudi.vasudhaika.net/rest/v1/profiles/otpusersignup");
	//static WebTarget webTarget = client.target("http://192.168.1.67/rest/v1/profiles/otpusersignup");
	static Invocation.Builder invocationBuilder = webTarget.request();
	static Thread thread = null;
	static DB db = null;
	static DBCollection inputCollection = null;
	static DBCollection masterCollection = null;
	static Gson gson = null;
	static volatile int numberOfUsersRegistered = 0, numberOfUserRegisterationsFailed = 0, numberOfUsersToRegisterInEachSet = 15, numberOfThreads = 1,
			initialNumberOfActiveThreads = 0, numberOfUsersAlreadyRegistered = 0;
	static long startTime = 0;
	static boolean populatingQueue = false, processData = true;
	static {
		try {
			db = MongoDBUtil.getConnection();
			inputCollection = db.getCollection("1OCTDUMP");
			masterCollection = db.getCollection("masterCollectionOCT1");
			//inputCollection = db.getCollection("PreCreatedDataInQueue");
			//masterCollection = db.getCollection("masterCollectionFinal4");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		/*if (args.length > 0)
			numberOfThreads = Integer.parseInt(args[0]);
		if (args.length > 1)
			numberOfUsersToRegisterInEachSet = Integer.parseInt(args[1]);*/
		initialNumberOfActiveThreads = Thread.activeCount();
		LOGGER.info(numberOfThreads + ",,,,,," + numberOfUsersToRegisterInEachSet + ",,,,,," + initialNumberOfActiveThreads);
		try {
			while (true) {
				populateQueue();
				if (startTime == 0)
					startTime = System.currentTimeMillis();
				if (queue.size() == 0)
					break;
				spawnThreads();
				while (true) {
					TimeUnit.SECONDS.sleep(60);//Sleeping the main thread so that this while loop doesn't consume much CPU.
					if (queue.size() == 0 && (Thread.activeCount() - initialNumberOfActiveThreads) == 0) {
						LOGGER.info("Finished waiting for queues and threads to become zero");
						break;
					}
				}
				if ((numberOfUsersRegistered + numberOfUserRegisterationsFailed + numberOfUsersAlreadyRegistered) >= (numberOfUsersToRegisterInEachSet * 1) && queue.size() == 0)
					break;
			}
			LOGGER.info("Abhishek signing off....");
		} catch (Exception e) {
			LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void populateQueue() {
		populatingQueue = true;
		LOGGER.info("\n\n\nStarted populating queues");
		long mongoGetStartTime = System.currentTimeMillis();
		DBCursor cursor = inputCollection.find(new BasicDBObject("status", AppConstants.IN_QUEUE)).limit(numberOfUsersToRegisterInEachSet);
		LOGGER.info("Mongo records fetching time is::: " + (System.currentTimeMillis() - mongoGetStartTime) + "ms");
		long queueFillingStartTime = System.currentTimeMillis();
		while (cursor.hasNext()) {
			RegistrationTo objRegistrationTo = gson.fromJson(cursor.next().toString(), RegistrationTo.class);
			queue.offer(objRegistrationTo);
		}
		LOGGER.info("Queue is filled in ::" + (System.currentTimeMillis() - queueFillingStartTime) + "ms");
		populatingQueue = false;
		LOGGER.info("Current queue size is :: " + queue.size());
	}

	@Override
	public void run() {
		LOGGER.info("Current thread count is::" + Thread.activeCount());
		while (!Thread.currentThread().isInterrupted()) {
			while (populatingQueue) {
				try {
					TimeUnit.SECONDS.sleep(5);
				} catch (InterruptedException e) {
					LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
					e.printStackTrace();
				}
			}
			if (numberOfUsersRegistered > 0 && numberOfUsersRegistered % 100 == 0) {
				LOGGER.info("Superman in work...");
				LOGGER.info(numberOfUsersRegistered + " users registered until now. Time elapsed is :: " + (System.currentTimeMillis() - startTime) + "ms\n\n\n");
			}
			if (queue.size() == 0) {
				while (Thread.currentThread().isInterrupted())
					Thread.currentThread().interrupt();
				if (Thread.activeCount() - initialNumberOfActiveThreads == 1) {
					LOGGER.info("Superman saves the day. Current data set is processed.");
					LOGGER.info("Total number of users registered successfully :: " + numberOfUsersRegistered);
					LOGGER.info("Total number of users who are already registered with us :: " + numberOfUsersAlreadyRegistered);
					LOGGER.info("Total number of user registrations failed :: " + numberOfUserRegisterationsFailed);
					LOGGER.info("Total time taken to process " + (numberOfUsersRegistered + numberOfUserRegisterationsFailed + numberOfUsersAlreadyRegistered) + " is :: "
							+ (System.currentTimeMillis() - startTime) + "ms");
				}
				break;
			}
			try {
				RegistrationTo objRegistrationTo = queue.poll();
				if (objRegistrationTo != null)
					hitPreCreatedUserEndpoint(objRegistrationTo);
			} catch (Exception e) {
				LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	private static void hitPreCreatedUserEndpoint(RegistrationTo registrationTo) {
		Response clientResponse = invocationBuilder.post(Entity.entity(gson.toJson(registrationTo), MediaType.APPLICATION_JSON));
		String response = clientResponse.readEntity(String.class);
		JSONObject jsonObject = new JSONObject(response);
		String responseCode = jsonObject.get("code").toString();
		List<BasicDBObject> query = new ArrayList<BasicDBObject>();
		query.add(new BasicDBObject("mobileNumber", registrationTo.getMobileNumber()));
		query.add(new BasicDBObject("status", new BasicDBObject("$ne", AppConstants.ACTIVE)));
		if (responseCode.equals("200")) {
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Success::" + jsonObject.get("info").toString());
			registrationTo.setStatus(AppConstants.ACTIVE);
			//String data = gson.toJson(registrationTo);
			//BasicDBObject basicDbObject = (BasicDBObject) JSON.parse(data);
			//WriteResult result = inputCollection.update(new BasicDBObject("mobileNumber", registrationTo.getMobileNumber()), basicDbObject, false, true);
			numberOfUsersRegistered++;
		} else {
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Error::" + jsonObject.get("error").toString());
			if (responseCode.equals("400")) {
				registrationTo.setStatus(AppConstants.FAIL_DUP_MOBILE);
				//updateMasterCollection(registrationTo, AppConstants.FAIL_DUP_MOBILE);
				numberOfUsersAlreadyRegistered++;
			} else if (responseCode.equals("500")) {
				registrationTo.setStatus(AppConstants.FAIL_EXCEPTION);
				//updateMasterCollection(registrationTo, AppConstants.FAIL_EXCEPTION);
				numberOfUserRegisterationsFailed++;
			}
			//BasicDBObject basicDBObject = (BasicDBObject) JSON.parse(gson.toJson(registrationTo));
		}
		inputCollection.update(
				new BasicDBObject("$and", query),
				new BasicDBObject("$set", new BasicDBObject("isPreCreatedRegComplete", true).append("preCreatedRegInfo", registrationTo.getPreCreatedRegInfo()).append("status",
						registrationTo.getStatus())), false, true);
		updateMasterCollection(registrationTo, registrationTo.getStatus());
	}

	private static void spawnThreads() {
		for (int i = 0; i < numberOfThreads; i++) {
			thread = new Thread(classObj, "TID : " + (i + 1));
			thread.start();
		}
	}

	private static void updateMasterCollection(RegistrationTo objRegistrationTo, String status) {
		/*MasterCollectionTo objMasterCollectionTo = getMongoRecordBasedOnMobileNumber(objRegistrationTo.getMobileNumber());
		System.out.println(gson.toJson(objMasterCollectionTo));*/
		/*objMasterCollectionTo.setStatus(status);
		objMasterCollectionTo.setPrfCreatedDate(getDate());
		objMasterCollectionTo.set_id(null);*/
		List<BasicDBObject> query = new ArrayList<BasicDBObject>();
		query.add(new BasicDBObject("mobileNum", objRegistrationTo.getMobileNumber()));
		query.add(new BasicDBObject("status", new BasicDBObject("$ne", AppConstants.ACTIVE)));
		masterCollection.update(new BasicDBObject("$and", query), new BasicDBObject("$set", new BasicDBObject("status", status).append("prfCreatedDate", getDate())), false, true);
	}

	public static String getDate() {
		DateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		toFormat.setLenient(false);
		return toFormat.format(new Date());
	}

	/*private static MasterCollectionTo getMongoRecordBasedOnMobileNumber(String mobileNo) {
		DBCursor cursor = masterCollection.find(new BasicDBObject("mobileNum", mobileNo));
		while (cursor.hasNext()) {
			MasterCollectionTo obj = gson.fromJson(cursor.next().toString(), MasterCollectionTo.class);
			return obj;
		}
		return null;
	}*/
}