package transaction.script.platform.startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

	public static final Logger LOGGER = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {
		String inputCase = args[0];
		switch (inputCase) {
		case "validatesheet":
			//ExcelSheetsValidator.validate(args);
			break;
		default:
			LOGGER.info("Invalid option program exiting....");
			System.exit(0);
		}
		LOGGER.info("Finished execution. Have a great day!!");
	}
}