package transaction.script.platform.startup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeSet;

import org.json.JSONObject;

import transaction.script.constants.AppConstants;
import transaction.script.platform.utils.AppFactory;
import transaction.script.platform.utils.DynamoConstants;
import transaction.script.platform.utils.MongoDBUtil;
import transaction.script.platform.utils.PostgresDBUtil;

import com.mongodb.DB;
import com.mongodb.DBCollection;

public class CorrectTransactionDataPrefix_Suffix {
	static DB db = null;
	static {
		try {
			db = MongoDBUtil.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws SQLException {
		TreeSet<String> databasesSet = new TreeSet<String>();
		CorrectTransactionDataPrefix_Suffix app = new CorrectTransactionDataPrefix_Suffix();
		try (Connection adminConnection = PostgresDBUtil.getConnection("postgres");
				Statement statement = adminConnection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT datname FROM pg_database WHERE datistemplate = false AND datname NOT IN ('postgres', 'commondb');");) {
			while (resultSet.next()) {
				databasesSet.add(resultSet.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String selectQuery = "SELECT tablename from pg_tables WHERE schemaname = 'public';";
		databasesSet.forEach(database -> {
			try (Connection userDbConnection = PostgresDBUtil.getConnection(database);
					Statement stmt = userDbConnection.createStatement();
					ResultSet rs = stmt.executeQuery(selectQuery)) {
				userDbConnection.setAutoCommit(false);
				StringBuffer countQuery = new StringBuffer("SELECT ");
				while (rs.next()) {
					switch (rs.getString(1)) {
					case AppConstants.TRADER_TABLE_NAME:
						countQuery.append("(SELECT COUNT(*) FROM kal_traderapp_transaction) as traderapp,");
						break;
					case AppConstants.CA_TABLE_NAME:
						countQuery.append("(SELECT COUNT(*) FROM kal_ca_transaction) as caapp,");
						break;
					case AppConstants.FARMER_TABLE_NAME:
						countQuery.append("(SELECT COUNT(*) FROM kal_farmerapp_transaction) as farmerapp,");
						break;
					case AppConstants.INPUTS_TABLE_NAME:
						countQuery.append("(SELECT COUNT(*) FROM kal_ai_transaction) as aiapp,");
						break;
					case AppConstants.SOCIETY_TABLE_NAME:
						countQuery.append("(SELECT COUNT(*) FROM kal_cos_transaction) as cosapp,");
						break;
					case AppConstants.TRANSPORTER_TABLE_NAME:
						countQuery.append("(SELECT COUNT(*) FROM kal_transporterapp_transaction) as transporterapp,");
						break;
					case AppConstants.WAREHOUSER_TABLE_NAME:
						countQuery.append("(SELECT COUNT(*) FROM kal_wh_transaction) as warehouse,");
					}
				}
				String businessType = app.getBusinessType(countQuery.replace(countQuery.length() - 1, countQuery.length(), ";").toString(), userDbConnection);
				if (businessType != null) {
					app.processTransaction(businessType, userDbConnection);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	private String getBusinessType(String countsQuery, Connection conn) {
		//System.out.println(countsQuery);
		if (countsQuery.length() > 10) {//Just being extra careful, might get a unknown type of database example "postgres" or "commondb". Have no idea about what is there in store in kalgudi.com
			try (Statement stmt = conn.createStatement(); ResultSet resultSet = stmt.executeQuery(countsQuery)) {
				while (resultSet.next()) {
					for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
						if (resultSet.getInt(resultSet.getMetaData().getColumnName(i)) != 0)
							return resultSet.getMetaData().getColumnName(i);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	private void processTransaction(String businessType, Connection conn) {
		String query = "", moduleId = "", postgresTableName = "", mongoCollectionName = "";
		switch (businessType) {
		case AppConstants.TRADER:
			query = "SELECT * FROM kal_traderapp_transaction";
			postgresTableName = AppConstants.TRADER_TABLE_NAME;
			mongoCollectionName = AppConstants.TRADER_COLLECTION_NAME;
			moduleId = DynamoConstants.TRADERAPP;
			break;
		case AppConstants.CA:
			query = "SELECT * FROM kal_ca_transaction";
			postgresTableName = AppConstants.CA_TABLE_NAME;
			mongoCollectionName = AppConstants.CA_COLLECTION_NAME;
			moduleId = DynamoConstants.BROKERAPP;
			break;
		case AppConstants.FARMER:
			query = "SELECT * FROM kal_farmerapp_transaction";
			postgresTableName = AppConstants.FARMER_TABLE_NAME;
			mongoCollectionName = AppConstants.FARMER_COLLECTION_NAME;
			moduleId = DynamoConstants.FARMERAPP;
			break;
		case AppConstants.INPUTS:
			query = "SELECT * FROM kal_ai_transaction";
			postgresTableName = AppConstants.INPUTS_TABLE_NAME;
			mongoCollectionName = AppConstants.INPUTS_COLLECTION_NAME;
			moduleId = DynamoConstants.AGRIINPUTAPP;
			break;
		case AppConstants.SOCIETY:
			query = "SELECT * FROM kal_cos_transaction";
			postgresTableName = AppConstants.SOCIETY_TABLE_NAME;
			mongoCollectionName = AppConstants.SOCIETY_COLLECTION_NAME;
			moduleId = DynamoConstants.COSAPP;
			break;
		case AppConstants.TRANSPORTER:
			query = "SELECT * FROM kal_transporterapp_transaction";
			postgresTableName = AppConstants.TRANSPORTER_TABLE_NAME;
			mongoCollectionName = AppConstants.TRANSPORTER_COLLECTION_NAME;
			moduleId = DynamoConstants.TRANSPORTERAPP;
			break;
		case AppConstants.WAREHOUSE:
			query = "SELECT * FROM kal_wh_transaction";
			postgresTableName = AppConstants.WAREHOUSER_TABLE_NAME;
			mongoCollectionName = AppConstants.WAREHOUSER_COLLECTION_NAME;
			moduleId = DynamoConstants.WAREHOUSEAPP;
			break;
		}
		try (Statement stmt = conn.createStatement(); ResultSet resultSet = stmt.executeQuery(query)) {
			while (resultSet.next()) {
				JSONObject objJsonObject = new JSONObject(resultSet.getString(2));
				if (!objJsonObject.has("prefix")) {
					String keyPatternId = objJsonObject.getString("keyPatternId");
					//objJsonObject.put("prefix", "");
					//objJsonObject.put("suffix", objJsonObject.getString("billNo"));
					String data = objJsonObject.toString();
					//System.out.println("this is postgres response" + data + "\n\n");
					//System.out.println(objJsonObject.getString("keyPatternId"));
					System.out.println("this is dynamo response::" + AppFactory.getDynamoApi().getValue(DynamoConstants.TRADERAPP, keyPatternId));
					Object dynamoResponse = AppFactory.getDynamoApi().createOrReplaceValue(moduleId, "operationName", data, keyPatternId);
					if (dynamoResponse != null) {
						try (PreparedStatement preparedStatement = conn.prepareStatement("UPDATE " + postgresTableName + " SET data = CAST(? AS jsonb) WHERE id = ?;")) {
							preparedStatement.setString(1, data);
							preparedStatement.setString(2, keyPatternId);
							preparedStatement.executeUpdate();
							DBCollection collection = db.getCollection("kal_traderapp_transaction");
							conn.commit();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}