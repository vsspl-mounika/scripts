/*
 * Copyright (c) 2014 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *
 * This code is the confidential and proprietary information of   
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with 
 * Vasudhaika Software Solutions Pvt Ltd.
 */
package transaction.script.platform.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;

public class DynamoDButil {
	static final Logger LOGGER = LoggerFactory.getLogger(DynamoDButil.class);

	public DynamoDB getConnection() {
		if (AppConstants.DYNAMO_FROM.equals("local")) {
			try {
				AWSCredentials credentials = new BasicAWSCredentials("dummy", "dummy");
				AmazonDynamoDBClient client = new AmazonDynamoDBClient(credentials);
				client.setEndpoint(AppConstants.DYNAMO_HOST);
				return new DynamoDB(client);
			} catch (Exception e) {
				LOGGER.error("<< Error connecting to DynamoDB Local:{}");
			}
		} else if (AppConstants.DYNAMO_FROM.equals("aws")) {
			try {
				return connect();
			} catch (Exception e) {
				try {
					Thread.sleep(500);
					return connect();
				} catch (Exception e1) {
					LOGGER.error("<< Error occured with thread intrruption:");
				}
				LOGGER.error("<< Error connecting to DynamoDB Remote Database");
			}
		}
		return null;
	}

	public DynamoDB connect() throws Exception {
		AWSCredentials credentials = new BasicAWSCredentials(AppConstants.AWS_ACCESSKEY, AppConstants.AWS_SECRETKEY);
		AmazonDynamoDBClient client = new AmazonDynamoDBClient(credentials);
		return new DynamoDB(client);
	}
}