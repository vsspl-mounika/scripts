package transaction.script.constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.platform.utils.PostgresDBUtil;

public class AppConstants {
	private static final Logger LOGGER = LoggerFactory.getLogger(PostgresDBUtil.class);
	static Properties properties;
	static {
		properties = new Properties();
		try {
			InputStream inputStream = AppConstants.class.getClassLoader().getResourceAsStream("platform.properties");
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error("<< Error loading properties file:");

		}
	}

	public static final String POSTGRES_IP = properties.getProperty("postgres_host");
	public static final String TRANSACTION = "TRANSACTION";
	//Dynamo Constants
	public static final String DYNAMO_ENVIRONMENT = properties.getProperty("dynamo_environment");
	public static final String DYNAMO_FROM = properties.getProperty("dynamo_from");
	public static final String DYNAMO_HOST = properties.getProperty("dynamo_host");
	public static final String AWS_ACCESSKEY = properties.getProperty("aws_accessKey");
	public static final String AWS_SECRETKEY = properties.getProperty("aws_secretKey");
	//Table Names for postgres
	public static final String TRADER_TABLE_NAME = "kal_traderapp_transaction";
	public static final String CA_TABLE_NAME = "kal_ca_transaction";
	public static final String FARMER_TABLE_NAME = "kal_farmerapp_transaction";
	public static final String INPUTS_TABLE_NAME = "kal_ai_transaction";
	public static final String SOCIETY_TABLE_NAME = "kal_cos_transaction";
	public static final String TRANSPORTER_TABLE_NAME = "kal_transporterapp_transaction";
	public static final String WAREHOUSER_TABLE_NAME = "kal_wh_transaction";
	//Collection Names for mongo
	public static final String TRADER_COLLECTION_NAME = "kal_traderapp_transaction";
	public static final String CA_COLLECTION_NAME = "kal_ca_transaction";
	public static final String FARMER_COLLECTION_NAME = "kal_farmerapp_transaction";
	public static final String INPUTS_COLLECTION_NAME = "kal_fertilizerapp_transaction";
	public static final String SOCIETY_COLLECTION_NAME = "kal_farmerapp_transaction";
	public static final String TRANSPORTER_COLLECTION_NAME = "kal_transporter_transaction";
	public static final String WAREHOUSER_COLLECTION_NAME = "kal_warehouseapp_transaction";
	//Biz Types
	public static final String TRADER = "traderapp";
	public static final String CA = "caapp";
	public static final String FARMER = "farmerapp";
	public static final String INPUTS = "aiapp";
	public static final String SOCIETY = "cosapp";
	public static final String TRANSPORTER = "transporterapp";
	public static final String WAREHOUSE = "warehouse";
	public static final String MONGO_HOST_ONE = properties.getProperty("mongo_host");
	public static final int MONGO_HOST_PORT = Integer.parseInt(properties.getProperty("mongo_port"));
	public static final String MONGO_HOST_TWO = properties.getProperty("mongo_host_two");
	public static final String MONGO_DATABASE = properties.getProperty("common_mongo_db_name");
	public static final String MONGO_USER = properties.getProperty("mongo_user");
	public static final String MONGO_PASSWORD = properties.getProperty("mongo_password");
	//Data Correction Constants
	public static final String DUPLICATE = "DUPLICATE";//Production status
	public static final String ACTIVE = "ACTIVE";//Production status
	public static final String DUPLICATE_ALL = "DUPLICATE_ALL";//Local mongo status
	public static final String DUPLICATE_DATA = "DUPLICATE_DATA";//Local mongo status
	public static final String DUPLICATE_MOBILE = "DUPLICATE_MOBILE";//Local mongo status
	public static final String IN_QUEUE = "IN_QUEUE";//Local mongo && production status
	public static final String FAIL_DUP_MOBILE = "FAIL_DUP_MOBILE";
	public static final String FAIL_EXCEPTION = "FAIL_EXCEPTION";
	public static final String DATA_SHEET = "DATA";
	//BusinessType Constants
	public static final String BUSINESS_TYPE_SHEETNAME_AGRITRADER = "agritrader";
	public static final String BUSINESS_TYPE_SHEETNAME_AGRIBROKER = "broker";
	public static final String BUSINESS_TYPE_SHEETNAME_FARMER = "farmer";
	public static final String BUSINESS_TYPE_SHEETNAME_TRANSPORTER = "transporter";
	public static final String BUSINESS_TYPE_SHEETNAME_AGRIINPUT = "agriinputs";
	public static final String BUSINESS_TYPE_SHEETNAME_AGRISOCIETY = "agricoop";
	public static final String BUSINESS_TYPE_SHEETNAME_WAREHOUSE = "warehouser";
	public static final String BUSINESS_TYPE_ID_AGRITRADER = "BT000000";
	public static final String BUSINESS_TYPE_ID_AGRIBROKER = "BT000001";
	public static final String BUSINESS_TYPE_ID_FARMER = "BT000002";
	public static final String BUSINESS_TYPE_ID_TRANSPORTER = "BT000003";
	public static final String BUSINESS_TYPE_ID_AGRIINPUT = "BT000008";
	public static final String BUSINESS_TYPE_ID_AGRISOCIETY = "BT000010";
	public static final String BUSINESS_TYPE_ID_WAREHOUSE = "BT000006";
	public static final String BUSINESS_TYPE_DBNAME_AGRITRADER = "Agri Trader";
	public static final String BUSINESS_TYPE_DBNAME_BROKER = "Broker (Intermediary)";
	public static final String BUSINESS_TYPE_DBNAME_FARMER = "Farmer";
	public static final String BUSINESS_TYPE_DBNAME_TRANSPORTER = "Transporter";
	public static final String BUSINESS_TYPE_DBNAME_WAREHOUSE = "Warehouser";
	public static final String BUSINESS_TYPE_DBNAME_INPUTS = "Agri Inputs";
	public static final String BUSINESS_TYPE_DBNAME_SOCIETY = "Agriculture Cooperative Society";
	//Validation constants
	public static final String INVALID_NAME = "Name invalid-";
	public static final String INVALID_BIZ_NAME = "Biz name invalid-";
	public static final String INVALID_MOBILE = "Mobile invalid-";
	public static final String INVALID_BIZ_TYPE = "Biz type invalid-";
	public static final String INVALID_LOCATION = "Location invalid-";
	public static final String INVALID_PRODUCT = "Products invalid-";
	public static final String INVALID_DATA = "Data is duplicate.";
	@SuppressWarnings("serial")
	public static final Map<String, String> MOBILE_TELECOM_CODES_LENGTH_MAP = Collections.unmodifiableMap(new HashMap<String, String>() {
		{
			put("+254", "9");
			put("+237", "9");
			put("+243", "9");
			put("+251", "9");
			put("+233", "9");
			put("+225", "9");
			put("+213", "9");
			put("+261", "9");
			put("+258", "9");
			put("+234", "10");
			put("+242", "9");
			put("+255", "9");
			put("+256", "9");
			put("+63", "10");
			put("+880", "10");
			put("+62", "9,10,11");
			put("+95", "8");
			put("+977", "10");
			put("+92", "10");
			put("+91", "10");
			put("+966", "9");
			put("+94", "7");
			put("+84", "10");
			put("+66", "9");
			put("+31", "9");
			put("+40", "9");
			put("+46", "9");
		}
	});
	@SuppressWarnings("serial")
	public static final Map<String, String> COUNTRY_CODES = Collections.unmodifiableMap(new HashMap<String, String>() {
		{
			put("Kenya", "+254");
			put("Cameroon", "+237");
			put("DemRepCongo", "+243");
			put("Ethiopia", "+251");
			put("Ghana", "+233");
			put("IvoryCoast", "+225");
			put("Algeria", "+213");
			put("Madagascar", "+261");
			put("Mozambique", "+258");
			put("Nigeria", "+234");
			put("RepCongo", "+242");
			put("Tanzania", "+255");
			put("Uganda", "+256");
			put("Philippines", "+63");
			put("Bangladesh", "+880");
			put("Indonesia", "+62");
			put("Myanmar", "+95");
			put("Nepal", "+977");
			put("Pakistan", "+92");
			put("India", "+91");
			put("SaudiArabia", "+966");
			put("SriLanka", "+94");
			put("Vietnam", "+84");
			put("Thailand", "+66");
			put("Netherlands", "+31");
			put("Romania", "+40");
			put("Sweden", "+46");
		}
	});
	@SuppressWarnings("serial")
	public static final Map<String, String> COUNTRY_IDS = Collections.unmodifiableMap(new HashMap<String, String>() {
		{
			put("Kenya", "101");
			put("Cameroon", "107");
			put("DemRepCongo", "112");
			put("Ethiopia", "117");
			put("Ghana", "120");
			put("IvoryCoast", "123");
			put("Algeria", "124");
			put("Madagascar", "128");
			put("Mozambique", "135");
			put("Nigeria", "138");
			put("RepCongo", "139");
			put("Tanzania", "150");
			put("Uganda", "153");
			put("Philippines", "301");
			put("Bangladesh", "303");
			put("Indonesia", "314");
			put("Myanmar", "329");
			put("Nepal", "330");
			put("Pakistan", "333");
			put("India", "335");
			put("SaudiArabia", "336");
			put("SriLanka", "340");
			put("Vietnam", "349");
			put("Thailand", "377");
			put("Netherlands", "436");
			put("Romania", "440");
			put("Sweden", "448");
		}
	});
}